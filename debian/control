Source: dovecot-fts-flatcurve
Section: mail
Priority: optional
Maintainer: Joseph Nahmias <jello@debian.org>
Build-Depends: debhelper-compat (= 13)
 , dovecot-dev
 , libxapian-dev
 , pkgconf
 , valgrind <!nocheck>
Standards-Version: 4.7.0
Homepage: https://github.com/slusarz/dovecot-fts-flatcurve
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/dovecot-fts-flatcurve
Vcs-Git: https://salsa.debian.org/debian/dovecot-fts-flatcurve.git

Package: dovecot-fts-flatcurve
Architecture: any
Provides: fts-flatcurve
Enhances: dovecot-imapd, dovecot-pop3d
Depends: ${shlibs:Depends}, ${misc:Depends}, dovecot-abi-${dovecot:ABI-Version}
Description: Dovecot full-text search plugin using the Xapian engine
 This is a Dovecot Full Text Search (FTS) plugin to enable message indexing
 using the Xapian Open Source Search Engine Library.
 .
 The plugin relies on Dovecot to do the necessary stemming. It is intended to
 act as a simple interface to the Xapian storage/search query functionality.
 .
 This driver supports match scoring and substring matches, which means it is
 RFC 3501 (IMAP4rev1) compliant; although substring searches are off by
 default. This driver does not support fuzzy searches, as there is no
 built-in support in Xapian for it.
 .
 The driver passes all of the ImapTest search tests.
 .
 Note: FTS Flatcurve will be integrated into Dovecot in v2.4, so this
 package is only needed for Dovecot v2.3.
